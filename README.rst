==================================
Your own easy to run gitlab-runner
==================================

Requirements
^^^^^^^^^^^^

* docker
* docker-compose

First run and set up
^^^^^^^^^^^^^^^^^^^^

* Run ``make env`` to generate example ``docker-compose.override.yml``
* Configure your ``REGISTRATION_TOKEN`` and ``RUNNER_NAME`` in ``docker-compose.override.yml``
* ``make run``
* ``make register``

Second run and on
^^^^^^^^^^^^^^^^^

As long as the runner is registered, just use:
``make run``

Notes
^^^^^

If computer reboots, runner containter is started automatically
To unregister the runner: 
``make unregister``
