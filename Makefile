.PHONY: help
help:
	@echo "Run 'make env' to generate example override file and then edit it!"
	@echo "Run 'make run' to run the runner container"

docker-compose.override.yml:
	@cp -v docker-compose.override.yml.example docker-compose.override.yml

volumes/runner/config/config.toml:
	@cp -v volumes/runner/config/config.toml.example volumes/runner/config/config.toml

.PHONY: env
env: docker-compose.override.yml volumes/runner/config/config.toml
	@echo "Now, edit docker-compose.override.yml to have your token and runner name"

.PHONY: run
run:
	docker-compose up -d

.PHONY: stop
stop:
	docker-compose down

.PHONY: register
register:
	docker-compose exec runner gitlab-runner register

.PHONY: unregister
unregister:
	docker-compose exec runner gitlab-runner unregister --all-runners
